---
title: Classes
layout: liste
collection: classes
inclureSource: true
colonnes:
  - titre: "Clef"
    attribut: "caracteristiqueClef"
  - titre: "pv"
    attribut: "pv"
  - titre: "Perception"
    attribut: "degrePerception"
  - titre: "Réf"
    attribut: "degreReflexes"
  - titre: "Vig"
    attribut: "degreVigueur"
  - titre: "Vol"
    attribut: "degreVolonte"
  - titre: "Nb compétences"
    attribut: "competences"
  - titre: "VO"
    attribut: "titleVO"
---


Liste des classes.