---
title: Ascendances
layout: liste
collection: ascendances
inclureSource: true
colonnes:
  - titre: "Pv bonus"
    attribut: "pvBonus"
  - titre: "Augmentations"
    attribut: "augmentations"
    estListe: true
    separateur: ", "
  - titre: "Pénalité"
    attribut: "penalite"
  - titre: "Langues"
    attribut: "langues"
    estListe: true
    separateur: ", "
  - titre: "VO"
    attribut: "titleVO"
---

Liste des ascendances.