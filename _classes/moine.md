---
title: Moine
titleVO: Monk
source: "Playtest Pathfinder"
layout: classe

#Partie propre aux classes
caracteristiqueClef: Sag
pv: 8

#Degrés: NE Ent Exp Maî Lég
degrePerception: Ent
degreVigueur: Exp
degreReflexes: Ent
degreVolonte: Exp

competences: 4
armes: "Ent pour toutes les armes simples et le cimeterre"
armures: "Ent pour les armures légères et intermédiaires non métalliques"
sorts: "Ent pour les jets et les DD des sorts primaires"
compSignature:
  - "Artisanat"
  - "Nature"
  - "Survie"
  - "une compétence dépendant de l'ordre"
---

Blah blah blah... description du druide

