---
title: Druide
titleVO: Druid
source: "Playtest Pathfinder"
layout: classe

#Partie propre aux classes
caracteristiqueClef: Sag
pv: 8

#Degrés: NE Ent Exp Maî Lég
degrePerception: Ent
degreVigueur: Exp
degreReflexes: Ent
degreVolonte: Exp

competences: 4
armes: "Ent pour toutes les armes simples et le cimeterre"
armures: "Ent pour les armures légères et intermédiaires non métalliques"
sorts: "Ent pour les jets et les DD des sorts primaires"
compSignature:
  - "Artisanat"
  - "Nature"
  - "Survie"
  - "une compétence dépendant de l'ordre"
---

Blah blah blah... description du druide



{:.bloc .action2}
> ### Forme élémentaire **Don 10**
> *Druide, Ordre sauvage*  
> **Conditions** Forme sauvage
> 
> ---
> Vous pouvez lancer forme élémentaire en dépensant un point de votre réserve
> de forme sauvage.  
> Test avec plusieurs paragraphes.


{:.bloc .action1}
> ### Forme élémentaire **Don 10**
> *Druide, Ordre sauvage*  
> **Conditions** Forme sauvage
> 
> ---
> Vous pouvez lancer forme élémentaire en dépensant un point de votre réserve
> de forme sauvage.  
> Test avec plusieurs paragraphes.  
> Ça a l'air de fonctionner. On peut aussi ajouter des résultats de jds.  
> **Échec critique** : t'es mort.  
> **Échec** : t'es à moitié mort.  
> **Réussite** : tu saignes juste un peu.  
> **Réussite critique** : tu n'as rien. 

