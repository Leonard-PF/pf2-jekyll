# Site règles Pathfinder 2de édition

<http://2nd.pathfinder-fr.org>

Ce projet contient le prototype du site PRD Pathfinder 2nde édition.

Le site peut être consulté à cette adresse : <http://2nd.pathfinder-fr.org>

Les contributeurs sont invités à consulter les [différents guide de contribution](https://gitlab.com/pathfinder-fr/pf2-jekyll/wikis/home).

Si vous avez constaté une erreur que vous souhaitez signaler, vous pouvez le faire en crééant une [issue](https://gitlab.com/pathfinder-fr/pf2-jekyll/issues) sur ce site.

## A Propos

Système basé sur :

* [jekyllrb.com](https://jekyllrb.com/) pour le site
* [kramdown](https://kramdown.gettalong.org/) pour la syntaxe et le format markdown