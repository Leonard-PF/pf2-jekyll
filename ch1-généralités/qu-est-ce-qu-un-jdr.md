---
title: "Qu'est-ce qu'un jeu de rôle ?"
source: "Playtest Pathfinder"
layout: default
---

WHAT IS A ROLEPLAYING GAME?
Pathfinder is a tabletop adventure roleplaying game (RPG):
an interactive story in which one player, the Game Master
(GM), sets the scene and presents challenges, while the
other players each assume the role of a sword and sorcery
adventurer, called a player character (PC), and attempt to
overcome those challenges. While many of these challenges
come in the form of monsters, devious traps, and the
machinations of adversarial agents, Pathfinder provides
enough depth to create challenges in the form of political
scheming, puzzle solving, interpersonal drama, and more.
The game is typically played in a group of four to
seven players, with one of those players serving as the
group’s Game Master. The GM prepares, presents,
and presides over the game’s world and its story,
posing challenges and playing adversaries, allies, and
bystanders alike. By responding to situations according
to their player characters’ personalities and abilities, the
players help to create the story’s plot as the outcome
of each scene leads into the next. Die rolls combined
with preassigned statistics add an element of chance
and determine whether characters succeed or fail at the
actions they attempt.
You can think of an RPG as theater: the players are the
actors, while the Game Master is the director. Whether you
choose to play a premade adventure, such as Pathfinder
Playtest Adventure: Doomsday Dawn, or an adventure of
your own making is up to you!
The Game’s Flow
Each time your group gets together to play Pathfinder,
that’s one game session. A complete game of Pathfinder
can be as short as a single session, commonly referred to
as a “one-shot,” or a game can stretch on for multiple
sessions in a campaign that lasts for months or even years.
A game of Pathfinder can be played for as long as the
Game Master has an ongoing story she enjoys exploring
and advancing with her players. As you continue to
play Pathfinder, the player characters will defeat vile
beasts, escape fiendish traps, and complete heroic quests.
When they do, they accumulate Experience Points, often
denoted as XP. Once a character reaches 1,000 XP, that
character gains a level. The higher your character’s level,
the greater the challenges they can face in the game!
The Players
Before the game begins, players typically invent their
own characters’ backgrounds and personalities. While it’s
possible to play multiple characters at once, it’s generally
the most fun to have one character per player so that
players can really get into their roles.
In the case of this playtest, Doomsday Dawn provides
suggestions about the types of characters best suited to
each of its parts. In some cases, the adventure requires
players to create specific types of characters, though these
requirements are strictly for the purposes of playtesting.
In general, Pathfinder limits character concepts only to the
players’ imaginations and the GM’s parameters.
Players use the game’s rules to build their characters’
numerical statistics, which determine the characters’
abilities, strengths, and weaknesses. In-depth instructions
for how to create a character, pointing you toward
relevant rules in other chapters, appear in the Character
Creation section on page 11.
During the game, the players describe the actions their
characters take. Some players particularly enjoy acting
(or roleplaying) the game’s events as if they were their
characters, while others describe their characters’ actions
as if narrating a story. Do whatever feels best!
Many in-game situations in Pathfinder have rules that
govern how they’re resolved. Chapter 9, for example,
provides detailed information about the different modes
of play, how to use the rules to spend actions when a
fight breaks out, and much more. All of the rules that
players need to play Pathfinder are found in this book.
Keep in mind, though, that this is a playtest and that
Pathfinder Second Edition’s final rules will be published
in August 2019 after we have gathered and incorporated
all of your feedback.
The Game Master
While the rest of the players create their characters for
a Pathfinder game, the Game Master is in charge of the
story and world. The Game Master is a player, but for the
sake of simplicity, she is referred to in this book and other
Pathfinder products as the Game Master or GM, whereas
the other players are referred to simply as players. The
Game Master must detail the situations she wants the
players to experience as part of an overarching story,
consider how the actions of the player characters might
affect her plans, and understand the rules and statistics
for the challenges they will face along the way. Game
Masters who use Doomsday Dawn will find that much
of this work is addressed in the adventure, though there’s
nothing wrong with participating in the playtest using
original adventures.
Gaming Is for All
Whether you’re a player or a Game Master, participating
in a tabletop roleplaying game involves an inherent social
contract: everyone has gathered to have fun together,
and the table is a safe space for everyone. Everyone has
a right to play and enjoy Pathfinder regardless of their
age, gender, race, religion, sexual orientation, or any other
identities and life experiences. Pathfinder is for everyone,
and Pathfinder games should be as safe, inclusive, and fun
as possible for all.
Players
As a player, it is your responsibility to ensure that you are
not creating or contributing to an environment that makes
any other players feel uncomfortable or unwelcome,
particularly if those players are members of minority
or marginalized communities that haven’t always been
welcome or represented in the larger gaming population.
Thus, it’s important to consider your character concepts and
roleplaying style and avoid any approach that could cause
harm to another player. A character whose concept and
mannerisms are racist tropes, for example, is exceptionally
harmful and works against the goal of providing fun for
all. A roleplaying style in which a player or character is
constantly interrupting others or treating certain players
or characters with condescension is similarly unacceptable.
Furthermore, standards of respect don’t vanish simply
because you’re playing a character in a fantasy game.
For example, it’s never acceptable to refer to another
person using an offensive term or a slur, and doing so
“in character” is just as bad as doing so directly. If your
character’s concept requires you act this way, that’s a good
sign your concept is harmful, and you have a responsibility
to change it. Sometimes, you might not realize that your
character concept or roleplaying style is making others
feel unwelcome at the gaming table. If another player
tells you that your character concept or roleplaying style
makes them uncomfortable, you shouldn’t argue about
what they should or shouldn’t find offensive or say that
what you’re doing is common (and therefore okay) among
players or in other media. Instead, you should simply stop
and make sure the game is a fun experience for everyone.
After all, that’s what gaming is about!
Game Masters
The role of Game Master comes with the responsibility of
ensuring that none of your players violate the game’s social
contract, especially when playing in a public space. Be on
the lookout for behavior that’s inappropriate, whether
intentional or inadvertent, and pay careful attention to
players’ body language during gameplay. If you notice
a player becoming uncomfortable, you are empowered
to pause the game, take it in a new direction, privately
check in with your players during or after the session, or
take any other action you think is appropriate to move
the game toward a fun experience for everyone. That
said, you should never let players who are uncomfortable
with different identities or experiences derail your game.
People of all identities and experiences have a right to be
represented in the game, even if they’re not necessarily
playing at your table.
Otherwise, if a player tells you they’re uncomfortable
with something in the game, whether it’s content you’ve
presented as the GM or another player’s actions, listen
to them and take steps to ensure they can once again
have fun during your game. If you’re preparing written
material and you find the description of a character or a
situation to be inappropriate, you are fully empowered
to change any details as you see fit to best suit your
players. Making sure the game is fun for everyone is your
biggest job!
HOW DO I PLAY?
Playing Pathfinder often involves meeting in someone’s
home or at a game store, but there are myriad virtual
tabletop applications—special programs or web
interfaces—that allow players to meet online to roll dice
and move pieces virtually. Some groups even mix the two,
meeting with some friends in person while having others
participate virtually. There is no wrong method to play
Pathfinder!
If you don’t have a local group, we encourage you
to seek a play-by-post game in the forums on the Paizo
website at paizo.com or a playtest at your local Pathfinder
Society Roleplaying Guild lodge. More about Pathfinder
Society can be found at paizo.com/pathfindersociety.
Several Pathfinder playtest products are designed to help
showcase the game’s new rules and to provide clear avenues
from which we can gather your feedback. These include this
rulebook, Doomsday Dawn, and the Pathfinder Playtest
Bestiary, which provides statistics for monsters, hazards,
and traps to populate your game. These products are all
available for free in PDF form at paizo.com. Additionally,
you can purchase print copies of this book and other
Pathfinder Playtest products at paizo.com or at your local
gaming store.
Other supplies for the playtest are described below.
Playtesting Supp lies
Each person participating in this playtest can bring their
own set of the following supplies, but it’s common for
members of a group to share some materials, like books,
pencils, and dice.
• This book.
• Pencils or other writing utensils.
• Character sheets (found on page 429 and at paizo.com).
• A set of polyhedral dice (see page 11 for a description of
these specialized gaming dice).
• Optionally, a miniature or pawn to represent your
adventuring character.
Game Master Supp lies
As well as a copy of this book, writing utensils, and dice,
the Game Master will need a few additional supplies:
• Doomsday Dawn, found at your local game store and
for free at paizo.com.
• Pathfinder Playtest Bestiary, available as a free PDF
download at paizo.com.
• Optionally, a large piece of paper or laminated mat
with a battle grid of 1-inch squares to use as a visual
aid for combat encounters. Pathfinder Playtest Flip-
Mat Multi-Pack is specifically designed for the playtest
adventure and is available at paizo.com and your local
gaming store.
• Optionally, miniatures, pawns, or chits to represent the
characters and monsters on the battle grid.
• Optionally, a Pathfinder Combat Pad to track the order
of combat.
