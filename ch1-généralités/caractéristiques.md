---
title: "Caractéristiques"
source: "Playtest Pathfinder"
layout: default
---

Your character has six ability scores that represent her
basic attributes and raw potential: Strength, Dexterity,
Constitution, Intelligence, Wisdom, and Charisma. These
scores each influence different aspects of your character’s
capabilities. As your character progresses through the
game’s story and increases in level, she gains ability boosts
that improve her ability scores, allowing you to decide
how her experiences have developed her raw capabilities.
THE SIX ABILITIES
Your ability scores are split into two main types: physical
and mental. Strength, Dexterity, and Constitution are
physical ability scores and measure your character’s
physical power, agility, and stamina. Intelligence,
Wisdom, and Charisma are mental ability scores and
measure your character’s learned prowess, reasoning, and
force of personality.
Strengt h
Strength measures your character’s physical power.
Strength is important if your character plans to engage
in the hand-to-hand fighting of melee combat. Fighters,
monks, paladins, and rangers all benefit from having a
high Strength score. Strength also determines how much
Bulk your character can carry (see page 175).
Dexterity
Dexterity measures your character’s agility, balance, and
reflexes. Dexterity is important if your character plans to
use stealth to surprise foes or to make attacks with ranged
weapons. Rogues in particular benefit from having a high
Dexterity score, as do rangers and some fighters and
monks. A high Dexterity score allows your character to
reduce or avoid attacks and magical effects that can be
dodged or outright evaded, such as dragon’s breath.
Constitution
Constitution measures your character’s health and
stamina. Constitution is an important statistic for all
characters, especially those who fight in close combat.
Fighters, monks, paladins, rangers, and rogues particularly
benefit from a solid Constitution score. Additionally,
Constitution grants characters extra Hit Points and makes
them more resistant against poison and disease.
Intelligence
Intelligence measures how well your character can learn
and reason. Intelligence is a vital statistic if your character
is an alchemist or a wizard, since these classes use
complicated formulas or spell notations when using their
abilities. A high Intelligence score allows your character
to perceive and understand patterns, and to pick up skills
that can serve her during her adventuring career.
Wisdom
Wisdom measures your character’s common sense,
awareness, and intuition. Wisdom helps you understand
religion and nature, and is a crucial statistic for clerics
and druids. A high Wisdom score can help your character
shake off mental spells and effects. Additionally, Wisdom
affects your character’s Perception modifier, which
measures how well she observes and identifies threats or
curiosities among her surroundings.
Charisma
This score measures your character’s strength of personality,
personal magnetism, and ability to influence the thoughts
and moods of others. Charisma is an important statistic
for bards and sorcerers, and it grants benefits to clerics
and paladins. Charisma also determines your character’s
Resonance Points (see page 376).
GENERATING ABILITY SCORES
When you’re ready to calculate your character’s ability
scores, follow the steps below. Your character’s ability
scores each start at 10, and then you’ll apply the ability
boosts and flaw she receives from her ancestry, background,
and class, plus four free ability boosts she receives at 1st
level. This process allows you to customize your character.
Ability Boosts
An ability boost is a one-time increase to a single ability
score. Typically, an ability boost increases an ability
score’s value by 2. However, if the ability score to which
you’re applying an ability boost is 18 or higher, its value
increases by only 1. At 1st level, a character can never
have any ability score that’s higher than 18.
When your character receives an ability boost, the rules
indicate whether it must be applied to a specific ability
score or one of two scores, or whether it is a “free” ability
boost that can be applied to any ability score of your
choice. When you gain multiple ability boosts at the same
time, you must apply each one to a different score. So,
for example, if your character is a dwarf, she receives an
ability boost to her Constitution score, her Wisdom score,
and one free ability boost, which can be applied to any
score other than Constitution or Wisdom.
When your character advances to 5th, 10th, 15th, and
20th levels, she receives ability boosts in four different
ability scores. These function similarly to the ability
boosts she receives at 1st level, but she is free to use them
to increase her ability scores above 18. Keep in mind
that once an ability score is 18 or higher, an ability boost
increases the value of that score by only 1.
Ability Flaws
Ability flaws are not nearly as common in Pathfinder as
are ability boosts. If your character has an ability flaw—
likely from her ancestry—it means she decreases that
ability score by 2 during Step 1 on page 19.

STEP-BY-STEP INSTRUCTIONS
The following instructions will help you calculate your
character’s ability scores at 1st level. You’ll start with a
value of 10 in each ability score, then make adjustments
for your character’s ancestry ability boosts and ability
flaws. This is followed by adding two ability boosts from
her background and four free ability boosts at 1st level.
Finally, she will receive one ability boost to her key ability
score from her class. After determining all of her ability
boosts, record the ability scores you’ve generated on your
character sheet. For information about modifying your
character’s ability scores as she gains levels, see Ability
Boosts on page 18 or the Ability Boosts sections of each
class entry in Chapter 3.
Step 1: Start with 10
On your character sheet (see pages 429-431) or a piece
of scratch paper, write down all six abilities—Strength,
Dexterity, Constitution, Intelligence, Wisdom, and
Charisma. Each ability starts with a score of 10.
Str 10, Dex 10, Con 10, Int 10, Wis 10, Cha 10
Step 2: Ancestry Ability Boosts and Flaws
Now apply the ability boosts that your character’s
ancestry provides. Most ancestries provide ability boosts
to two specific ability scores and another free ability
boost. (Humans are an exception—they receive two free
ability boosts instead.) When you apply these ability
boosts, keep in mind that you must apply each one to a
different score.
Then, apply the ability flaw your character’s ancestry
requires, if any. An ability flaw reduces that ability score
by 2. You can apply your free ability boost to that score
to bring your score back to 10.
For example, let’s say you’re making a human
character, and you want her to be a fighter. Human
characters receive two free ability boosts, and you
want to make a fighter who bashes skulls and can take
a beating. For that, you’ll want a good Strength and
Constitution. Remember that you can’t apply multiple
ability boosts from the same source to a score at a time,
so you couldn’t have a Strength score of 14 quite yet.
After the ancestry step, your ability scores might look
like this:
Str 12, Dex 10, Con 12, Int 10, Wis 10, Cha 10
As a second example, if you were making a dwarven
fighter, you’d receive ability boosts to Constitution and
Wisdom, plus one free ability boost, but you’d have an
ability flaw in Charisma. Your ability scores would start
out like this:
Str 12, Dex 10, Con 12, Int 10, Wis 12, Cha 8

OPTIONAL: VOLUNTARY FLAWS
Sometimes, it’s fun to play a character with a major flaw even if
you’re not playing an ancestry that automatically starts with one.
If you want to reduce any ability scores for your character below
what they would normally start at, that’s fine—playing a brutish
barbarian with an Intelligence score of 6 or a sickly wizard with
a Constitution score of 4 could allow for some fun roleplaying
opportunities—but you don’t get any benefit from taking on these
voluntary flaws. Beware of making your scores so low that your
character can’t keep up with the party!

Step 3: Two Background Ability Boosts
Once you’ve applied your character’s ability boosts and
ability flaw, if any, from her ancestry, it’s time to apply
her ability boosts from her background. (Descriptions of
Pathfinder’s character backgrounds begin on page 38.)
The background you choose gives your character two
ability boosts. Usually, this consists of one free ability
boost and one ability boost limited to certain choices by
the background.
Let’s say you want your human fighter to have been
a hardworking farmhand in her past. The farmhand
background says your character gets an ability boost that
must be applied to either Constitution or Wisdom. You
want your fighter to be hardy, so you pick Constitution.
Next, for your character’s free ability boost, you decide
to keep increasing her Strength. Now your ability scores
are the following:
Str 14, Dex 10, Con 14, Int 10, Wis 10, Cha 10
If you were making the dwarven fighter we mentioned
earlier into a farmhand, you’d instead have these scores:
Str 14, Dex 10, Con 14, Int 10, Wis 12, Cha 8
Step 4: Four Free Ability Boosts
After you’ve chosen your character’s ancestry and
background, you have four free ability boosts you can
assign to her ability scores as you see fit. These represent
your character’s variety of experiences growing up
before she became an adventurer.
Let’s take the sample human fighter from above. We
know you want your fighter to be strong, so you decide
to spend one of this step’s free ability boosts on Strength.
Dexterity is important for defense, so you put one
into Dexterity. You want to keep making your fighter
tough, so you apply an ability boost to Constitution,
too. Wisdom affects your defense against most mental
magic, and since a strong, tough fighter is the last person
you want controlled by some evil wizard, you decide to
put your last ability boost into Wisdom. After the four
ability boosts, your ability scores look like this:
Str 16, Dex 12, Con 16, Int 10, Wis 12, Cha 10
Let’s examine how this step might play out for the
dwarven fighter mentioned above. You decide you’ll
have fun roleplaying an uncharismatic character,
so you stick with the 8 in Charisma. You apply the
ability boosts to Strength, Dexterity, Constitution, and
Wisdom, just like the human fighter. Your ability scores
now look like this:
Str 16, Dex 12, Con 16, Int 10, Wis 14, Cha 8
Step 5: One Class Ability Boost
Finally, your choice of your character’s class will give
you one ability boost in a score that’s important to your
class’s abilities. This ability score is called your class’s
key ability. Most of the class write-ups in Chapter 3
indicate a specific score you apply this ability boost to
when you choose that class, but some let you select from
one or two choices.
Because our sample character is a fighter, you get to
choose Strength or Dexterity for your character’s class
ability boost. Your character’s Dexterity isn’t doing
so hot, and it might be a liability, but you don’t want
to miss out on having a Strength score of 18, so that’s
where you put your final ability boost. If you’re really
worried about your Dexterity, you could go back and
reassign an ancestry ability boost from Constitution
to Dexterity, or change your background to one that
allows Strength or Dexterity as a choice. Assuming you
stick with your burly character, though, your ability
scores look like this:
Str 18, Dex 12, Con 16, Int 10, Wis 12, Cha 10
Our dwarf picks Strength, too:
Str 18, Dex 12, Con 16, Int 10, Wis 14, Cha 8
Step 6: Record Ability Scores and Modifiers
Once you’ve determined your ability scores, write
them down in the appropriate boxes on your character
sheet. You’ll then want to reference Table 1–4: Ability
Modifiers below to determine the ability modifier
tied to each of your ability scores. Record this ability
modifier in the box just to the left of each ability score.
If you ever need to calculate an ability modifier on
the fly, it’s a fairly easy formula: simply subtract 10
from the ability score, then divide the result by two
(rounding down).
TABLE 1–4: ABILITY MODIFIERS
Ability Score Modifier
1 –5
2–3 –4
4–5 –3
6–7 –2
8–9 –1
10–11 +0
12–13 +1
14–15 +2
16–17 +3
18–19 +4
20–21 +5
22–23 +6
24 +7

OPTIONAL: ROLLING ABILITY SCORES

The standard method of generating ability scores described
earlier in this section works great if you want to create a perfectly
customized, balanced character. But your GM may decide to inject a
little randomness into character creation and elect to let dice decide
what kind of character the players are going to play. In this case,
you can use this alternative method to generate your ability scores.
Be warned—the same randomness that makes this system fun also
allows it to sometimes create characters that are significantly more
(or less) powerful than the standard ability score system does or
other Pathfinder rules assume. If your GM opts for rolling ability
scores, follow these alternative steps.
17s and 18s
If you reach a 17 or 18 in an ability score, you’re still limited to the cap
of 18 in any ability score at level 1. This might mean a step will require
you to put an ability boost into an ability that would raise it above the
cap. When this happens, you can put the ability boost into another
score instead, or you can put it into a 17 and lose the excess increase.
For instance, if you rolled a 17 for Constitution and were creating a
dwarf, you could apply your dwarf’s racial Constitution ability boost
to your Strength of 14 instead. You could still apply it to Constitution
anyway, making it an 18, but you’d lose the long-term benefits you
would have gained if you’d applied it to a lower score.

Step-by-Step Instructions
Step 1: Roll Base Ability Scores
Roll four six-sided dice (4d6) and discard the lowest die result. Add the three remaining results together and record the sum on a
piece of scratch paper. Repeat this process until you’ve generated six such values.
Step 2: Assign Values to Ability Scores
Pick which of these values will become each of your ability scores. For example, let’s again assume you are creating a fighter that’s
both strong and tough, and you have received the following values from rolling:
17, 14, 13, 11, 9, 9
You assign them to your ability scores as follows. At this stage, the human and dwarven fighters would have the same scores.
Str 17, Dex 13, Con 14, Int 9, Wis 11, Cha 9
Step 3: Apply Ancestry Ability Boosts and Flaws
Next you apply the ability boosts your character gains from her ancestry, but she receives one fewer free ability boost than normal.
If your character’s ancestry has any ability flaws, you apply those at this point, as well. Our human fighter would have only one free
ability boost using this method. After putting the ability boost into Dexterity, your ability scores would look like this:
Str 17, Dex 15, Con 14, Int 9, Wis 11, Cha 9
Our dwarf would get two ability boosts (instead of three), which must go into Constitution and Wisdom. She does not receive a
free ability boost, but she must still reduce her Charisma by 2 because of her ability flaw. The adjusted scores would be as follows:
Str 17, Dex 13, Con 16, Int 9, Wis 13, Cha 7
Step 4: Apply Background Ability Boost
With the rolling method of ability score generation, your character loses the free ability boost from her background. You can still
boost your human farmhand’s Constitution or Wisdom score. Selecting Constitution, the human farmhand would have these scores:
Str 17, Dex 15, Con 16, Int 9, Wis 11, Cha 9
The dwarven farmhand would have these scores:
Str 17, Dex 13, Con 18, Int 9, Wis 13, Cha 7
Step 5: Record Ability Scores
In the rolling method, you don’t get free ability boosts or an ability boost from your class. Write down your scores and modifiers, just
as you would if you had used the standard method of generating ability scores described earlier in this section.