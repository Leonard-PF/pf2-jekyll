---
title: "Concepts de base"
source: "Playtest Pathfinder"
layout: default
---

Pathfinder est un jeu riche rempli de nuances tant dans les règles que dans les histoires qu'il permet de
raconter. Voici quelques concepts de base avec lesquels il peut être utile de vous familiariser avant de jouer
ou de faire jouer une session de Pathfinder.

## Lire ces règles

À travers ces règles, vous rencontrer des informations formattées d'une manière qui peut paraître étrange à
première vue. Ces formats sont utilisés pour faciliter la lecture. Par exemple, les éléments de règle du jeu se
distinguent du reste du texte grâce à l'utilisation de majuscules ou de texte italique.

Les noms des caractéristiques, des compétences, des dons, des actions, des activités, des réactions, des actions
libres et certains autres éléments de règles de Pathfinder commencent par une majuscule. Grâce à cela, quand vous
voyez une phrase telle que "Une Frappe cible la Classe d'Armure," vous savez que les mots Frappe et Classe d'Armure
sont à prendre au sens décrits dans les règles. Pathfinder utilise églement de nombreux termes qui sont
généralement présentés sous forme abrégée. Si un terme commençant par une majuscule ou une abréviation vous fait
hésiter, vous pouvez toujours vous référer au <mark>glossaire</mark> ou à <mark>l'index</mark> pour en apprendre plus.

Si un mot ou un groupe de mots est en italique, il désigne un sort ou un objet magique. Ainsi, quand vous lisez "il est
protégé par *bouclier*," vous savez que le mot *bouclier* désigne le sort plutôt qu'un objet bouclier. Les descriptions des sorts se trouvent dans <mark>le chapitre 7</mark> alors que les objets magiques se trouvent au <mark>chapitre 12</mark>.

Ce livre est divisé en chapitres qui contiennent des informations pour créer un personnage, jouer à Pathfinder et
construire le monde de jeu. Plusieurs de ces chapitres contiennent des règles plutôt longues ou des blocs de
description technique. Là où c'est pertinent, la description des formats utilisés dans ces règles ou ces blocs
techniques est donnée juste avant eux. Par exemple, la section sur les ascendances du <mark>Chapitre 2</mark> contient
des pour chacune des 6 ascendances de base et une explication de ces règles est fournie juste au début de ce chapitre.

## Modes de jeu

Il y a trois modes de jeu principaux à Pathfinder : les rencontres, l'exploration et le temps libre. Ces différents modes
de jeu sont décrits en détail <mark>à partir de la page 290</mark> mais connaître les éléments de base suivants peut
être utile.

Le mode "rencontres" est constitué de rounds pendant lesquels chacun des joueurs peut, à son tour, utiliser des actions
pour aider le groupe à atteindre ses objectifs, qu'il s'agisse de se faufiler discrètement à travers une foule ou de
vaincre des ennemis. C'est le mode de jeu le plus rigoureux au niveau des mécaniques de jeu et il est surtout utile
pour gérer des combats ou quand une situation peut changer d'un moment à l'autre.

Le mode "exploration" se déroule quand les personnages parcourent des distances importantes, explorent de nouveaux
lieux mystérieus, intéragissent avec des PNJ autrement qu'au cours d'un combat ou sont tout simplement à l'affût
de tout danger. Dans ce mode, le temps passe au rythme que le MJ décide. Les joueurs disposent toujours d'occasions
pour influencer le monde de jeu mais d'une manière beaucoup plus libre. Le mode "exploration" se déroule entre les
rencontres et permet souvent de planter le décor pour un moment de jeu round par round.

Le "temps libre" se déroule quand les personnages ne sont pas confrontés à des dangers imminents, généralement quand
ils sont en sécurité, à l'abri dans une zone civilisée. Cela leur donne l'occasion de s'entraîner, de pratiquer
un métier et de profiter de la vie autrement qu'en participant à des aventures périlleuses.

Ces trois modes de jeu sont distincts mais les passages de l'un à l'autre ne sont pas toujours fort évidents. Il est
possible qu'une journée qui commence avec du temps libre se poursuive avec l'exploration des égoûts de la ville, ce
qui mène les personnages vers la base secrète d'un culte et vers une rencontre avec les membres de ce culte. Plus vous
jouez à Pathfinder et plus vous constaterez que chacun de ces modes possède ses méthodes de jeu propres mais que passer
de l'un à l'autre se fait sans obstacle.




standards that might look a bit unusual at first. These
standards are in place to make this book easier to read.
Specifically, the game’s rules are set apart in this text using
specialized capitalization or italicization.
The names of specific statistics, skills, feats, actions,
activities, reactions, free actions, and some other
mechanical elements in Pathfinder are capitalized. This
way, when you see the statement “a Strike targets Armor
Class,” you know that both Strike and Armor Class
are used with their rules meanings. Pathfinder also uses
many terms that are typically expressed as abbreviations.
If you’re ever confused about a capitalized term or an
abbreviation, you can always turn to the glossary or index,
found on pages 420 and 424, and look it up.
If a word or a phrase is italicized, it is describing a spell
or a magic item. This way, when you see the statement
“he is protected by shield,” you know that in this case
the word denotes the shield spell, rather than a shield, the
item. The descriptions of spells are found in Chapter 7,
beginning on page 203, while magic items are found in
Chapter 12, beginning on page 380.
This book is divided into chapters that contain information
for creating a character, playing the game, and building the
game’s world. Many of these chapters contain lengthy rules
or stat blocks. Where appropriate, the explanation of the
format of these rules or stat blocks appears before them.
For example, the Ancestry section of Chapter 2, which
begins on page 22, contains rules entries for each of the
game’s six core ancestries, and an explanation of these rules
appears at the beginning of the chapter.
MODES OF PLAY
There are three main modes of play in a Pathfinder game:
encounter mode, exploration mode, and downtime mode.
The different modes of play are discussed in more detail
starting on page 290, but knowing the following basics
before beginning a game is helpful.
Encounter mode is made up of rounds, during which
each player in turn can spend actions to advance the group’s
goals, whether the party wants to sneak past a crowd or
defeat some enemies. This is the most mechanically rigorous
mode of play and works best when running combat or
when a situation changes from moment to moment.
Exploration mode happens when the characters
travel significant distances, delve into mysterious new
locations, interact with nonplayer characters outside of
combat or simply watch for danger. In this mode, time
moves at whatever pace the GM sees fit. Players still have
opportunities to affect the game world, though in a much
more free-form manner. Exploration mode takes place
between encounters and often sets the stage for roundby-
round play.
Downtime takes place when the characters aren’t
facing any active threats. Downtime typically happens
while the characters are within the safety and security of
a settlement, and it allows them to train, employ a trade,
and experience life beyond their perilous adventures.
These three modes are distinct, but the game’s flow
between them isn’t always clear-cut. It’s possible that a
day that starts with downtime will involve the exploration
of the city’s sewage tunnels, leading the characters to the
secret base of a depraved cult and then into an encounter
with the cultists. The more you play the game, the more
you’ll see that each mode features of its own play methods,
but moving from mode to mode has few hard boundaries.
ACTING AND EFFECTS
The main way that characters and their adversaries
affect the world of Pathfinder is by spending actions and
producing effects. This is especially the case in encounter
play, where every action counts.
During encounter mode, each player character gains
3 actions and 1 reaction to use each round. Each player
character can also take any number of free actions. Rules
elements that describe actions, reactions, or free actions
carry special symbols to indicate this, as described below.
Actions
Actions start with this symbol: . You can use 3 actions
on your turn, in any order you see fit. When you use an
action, you generate an effect. Sometimes this effect is
automatic. Other times, actions necessitate that you roll
a die and generate an effect based on this roll. More
information about die rolling and its importance to the
game is found in the Die Rolls section on page 8.
Reactions
Reactions start with this symbol: . Reactions are similar
to actions in their effects, but are used differently. You can
spend only 1 reaction per round, and only when its specific
trigger is fulfilled. You can use your reaction on your turn
or another character’s turn, as long as the trigger is satisfied
(often, the trigger is another creature’s action).
Free Actions
Free actions start with this symbol: . Free actions follow
the same trigger rules as reactions, and like reactions, you
can use them on your turn or on another character’s turn.
Unlike with actions or reactions, you can use as many
free actions per round as you like, as long as their triggers
occur. However, a specific trigger can trigger only one of a
character’s free actions.
Activities
Activities are special tasks that you complete by spending
1 or more actions. Usually, they take 2 or more actions and
let you do more than a single action would allow. All tasks
that take longer than a turn are activities. Spellcasting is
one of the most common activities, as most spells require
you to spend several actions in a sequence to create their
effects. Once you spend the last action required, your
activity is complete and its effects occur. A few activities
can be performed by spending a free action or a reaction,
such as spells you can cast in an instant. Activities that use
2 actions start with this symbol: . Activities that use 3
actions start with this symbol: .
FORMAT OF RULES ELEMENTS
In Pathfinder, player characters use rules elements to act
or in response to the situation. Many are feats, which are
mechanical elements that characters can access through
leveling up or making certain character creation choices.
Others are more basic elements of the game to which all
characters have access from the beginning of play.
Regardless of the type of game mechanic, rules elements
are always presented in the following format. Entries are
omitted from the game’s rules text when they don’t apply,
so not all rules elements have all of the entries given below.
Actions, reactions, and free actions have an appropriate
icon next to them to indicate their type. An activity that
can be completed in a single turn has a number of icons
indicating how many actions are required to complete
it; activities that take more than a single turn to perform
omit these icons. When a certain level is required before
the element can be accessed, that level is indicated to the
right of the name. Rules also often have one or more traits
associated with them; for more about traits, see page 10.
FEAT OR ACTION NAME LEVEL
Prerequisites Any minimum ability score, feats,
proficiency rank, or other prerequisites you must have before you
can access this element is listed here. The element’s level is always
an additional prerequisite.
Frequency This is the limit on how many times you can use an
ability within a certain length of time.
Cost Any extra cost of materials to use the ability (for instance, in
spells that require special reagents) is listed here.
Trigger Reactions and free actions both have triggers that must
be met in order to use them. The trigger is listed here.
Requirements Sometimes you must have a certain item or be in a
certain circumstance to use an ability. If so, it’s listed in this section.
This section describes the effects or benefits of a rules element.
If the rule is an action, activity, reaction, or free action, it explains
what the effect is or what you must roll to determine the effect.
If it’s a feat that modifies an existing action or grants a constant
effect, the benefit is explained here.
Special Any special qualities of the rule are explained in
this section. Usually the special section appears in a feat you can
select more than once, and explains what happens when you do.
Traits
DIE ROLLS
Rolling dice to determine a character’s success or failure at
a given task is a core element of the game. Pathfinder uses
a set of polyhedral dice to accomplish this; most important
is the 20-sided die, often abbreviated as d20. Many rolls
in Pathfinder involve rolling a d20, adding bonuses or
penalties, and telling the GM the result so she can compare
it to the number representing the difficult of the task. This is
called a check. More about rolls, checks, bonuses, penalties,
and other key elements of the game is described beginning
on page 290, but the following basics are good to know:
• Rolling high is good. You want your roll’s total to meet
or exceed the threshold for success, which is called the
Difficulty Class (or DC). The DC is a number chosen by
the GM if you’re going up against a challenge in the
environment. If you’re rolling against a creature, you’ll
instead use a DC based on one of its statistics. More about
Difficulty Classes is on page 291.
• Rolling 20 is better! Rolling a 20 on the die means you
critically succeed, which often has a greater effect than
normal. You also gain a critical success if your total meets
or exceeds the Difficulty Class by 10 or more. More about
critical successes is on page 292.
• Rolling 1 is bad. When you do, you critically fail your check.
This is even worse than a regular failure. You also critically
fail if your total is lower than the Difficulty Class by 10 or
more. More about critical failures is on page 292.
Your Diff iculty Class
Often, you roll dice against a Difficulty Class determined
by the GM. But when a creature or situation is testing your
character’s ability, it attempts a check against a Difficulty
Class based on the most relevant of your character’s
statistics. The DC for any statistic is 10 plus all the same
modifiers you’d add to a d20 roll using that statistic.
Modifiers, Bonuses, and Penalties
You can get the following types of modifiers, bonuses, and
penalties on your rolls and DCs. More about modifiers,
bonuses, and penalties can be found on page 290.
Ability Modifier
Almost all rolls are keyed to an ability score. Your
ability modifiers are derived from your ability scores, as
described on page 20. For example, a Strength of 10 gives
you a +0 Strength modifier, while a Charisma of 17 gives
you a +3 Charisma modifier. You add only one ability
modifier to a roll.
Proficiency Modifier
For most of your statistics, your class and other character
choices will give you a proficiency rank. When you make a
roll, you add a proficiency modifier that depends on your
level and your proficiency rank in the statistic or item you
are using. You add only one proficiency modifier to a roll.
You’re untrained if you have little or no knowledge in
the statistic or item. Your proficiency modifier is equal to
your level minus 2. Unless your class or another choice
you’ve made gives you a different rank of proficiency,
you’re untrained.
If you’ve been trained in the statistic or item, your
proficiency modifier is equal to your level.
As an expert, you are highly trained in the statistic or
item. Your proficiency modifier is equal to your level plus 1.
At a master rank, you’ve achieved world-class
proficiency in the statistic or item. Your proficiency
modifier is equal to your character level plus 2.
If you’re legendary, your statistic or familiarity with
the item is so high that you’ll go down in history. Your
proficiency modifier is equal to your level plus 3.
When you have a proficiency rank, you also have all
lower ranks except for untrained. Thus, if you have master
proficiency rank, you also have the expert and trained
proficiency ranks (though you use only the highest modifier).
Bonuses and Penalties
Other bonuses and penalties come in several types. If you
have more than one bonus or penalty of the same type,
you use only the highest bonus or penalty.
KEY TERMS
As you navigate this book, it’s helpful to be familiar
with the key basic terms found in this section. For a full
glossary of game terms, see page 420.
Ability Score
Each creature has six ability scores: Strength, Dexterity,
Constitution, Intelligence, Wisdom, and Charisma. These
scores represent a creature’s raw potential and basic
attributes. The higher the score, the greater the creature’s
potential in that ability score. You can learn more about
ability scores on page 18.
Alignment
Alignment represents a creature’s basic moral and ethical
attitude. See page 16 for more details.
Ancestry
An ancestry is the broad family of people that a character
or creature belongs to. Ancestry determines a character’s
starting Hit Points, languages, Speed, and senses, and it
grants access to a set of ancestry feats.
Armor Class (AC )
All creatures in the game have an Armor Class. This score
represents how hard it is to hit and damage a creature. It
typically serves as the Difficulty Class for hitting a creature
with an attack. Learn more on page 16.
Att ack
When a creature tries to harm another creature, it makes
an attack roll against the target’s Armor Class. Most
attacks use the Strike action (see page 308), but some
spells and other abilities are also attacks.
Class
A class represents the main adventuring profession chosen
by a character. A character’s class determines most of their
proficiencies, grants the character a certain number of Hit
Points when they gain a new level, and gives access to a set
of class feats. Classes appear in Chapter 3.
Condition
An ongoing effect that changes how you can act or alters
some of your statistics is called a condition. These often
come from spells. Some frequently occurring conditions
appear on page 320.
Feat
A feat is an ability that a character gains from their ancestry,
background, class, general training, or skill training. Some
feats grant an action, activity, free action, or reaction.
Game Master (GM)
The Game Master is the player who adjudicates the rules
and controls the various elements of the Pathfinder story
and world that the other players explore. A GM’s duty
is to provide a fair and fun game—she wants the other
players to ultimately succeed in their goals, but only after
much danger and many heroic trials.
Golarion
Pathfinder is set on Golarion, a world with a rich
diversity of peoples, filled with exciting locations to
explore and deadly villains to challenge heroes of any
level. Throughout this book there are references to
Golarion, most notably in discussion of the deities that
your character can choose to venerate. You can set your
game in Golarion or any other world of your choosing,
using these rules to create a rich setting all your own. For
more information on Golarion, see Pathfinder Campaign
Setting: The Inner Sea World Guide and other books in
the Pathfinder Campaign Setting line.
Hit Points (HP)
Hit Points represent the amount of punishment a creature
can take before it falls unconscious and begins dying.
Damage decreases Hit Points on a 1-to-1 basis, while
healing restores Hit Points at the same rate. Learn more
on page 294.
Initiative
At the start of an encounter, all creatures involved roll
for initiative to determine the order the participants act
during combat. The higher the result of the roll, the earlier
a creature gets to act. Usually you’ll use Perception for
your initiative rolls, but you might roll some kind of skill
check instead. Learn more on page 304.
Level
A level is a number that measures something’s overall power.
A character has a character level, ranging from 1 to 20,
representing their level of experience. Monsters, hazards,
and afflictions have levels from 0 to 20 or higher that
measure the danger they pose. An item’s level indicates its
power and suitability as treasure, also from 0 to 20 or higher.
Spells have levels ranging from 1 to 10, which measure
their power and relate to a creature’s ability to cast them.
Nonplayer Character (NPC)
A nonplayer character is controlled by the GM for the
purpose of interacting with players and helping advance
the story.
Percept ion
Perception measures your character’s ability to notice
hidden objects or unusual situations, and it usually
determines how quickly you spring into action in combat.
Learn more on page 301.
Player Character (PC)
This is a character created and controlled by a player.
Rarity
Nearly all elements of the game are associated with a
rarity—that is, how often they’re encountered in the
game’s world. Rarity primarily applies to equipment and
magic items, but spells, feats, and other mechanical aspects
of the game also each have a specific rarity.
The majority of such elements are commonly found
within the world, which means that anyone can buy
them, in the case of items, or access them, in the case of
feats, without any trouble. The common rarity, marked in
black, is the default.
The uncommon rarity indicates an element available
only to those who have been initiated into a special kind
of training, grow up in a certain culture, or come from a
particular part of the world. A character can’t take these
options by default. Specific choices, such as class features
or backgrounds, might give access to certain uncommon
elements. The GM can grant any character access to
uncommon options if she so chooses. The level (or type of
element for those without levels) is marked in red.
Elements that are rare are practically unknown or
impossible to find in the game world. These elements
appear in the game only if the GM chooses to include
them. Rare elements are marked in orange.
The unique rarity indicates an element that is one of
a kind. This means that there’s only one in the game’s
world. Artifacts, for example, are often unique. Unique
elements are marked in blue (one appears in Pathfinder
Playtest Adventure: Doomsday Dawn).
Roleplaying
Describing a character’s actions, often while acting from
the perspective of the character, is called roleplaying. When
a player speaks or describes action from the perspective of
a character, they are “in character.”
Saving Throw (Save)
When a creature is subject to dangerous effects it must
attempt to avoid with its body or mind, it can often roll
a saving throw to mitigate the effect. You roll a saving
throw automatically—you don’t have to use an action
or a reaction. Unlike most types of checks, the character
who isn’t acting rolls the d20 for a saving throw and the
creature who is acting generates the DC.
There are three types of saving throws: Fortitude (to
resist poisons, diseases, and physical effects), Reflex (to
mitigate effects that you could quickly dodge), and Will
(to resist effects that target the mind and personality). You
can learn more on page 17.
Skill
A skill represents a creature’s ability to perform certain
tasks that require training to master. Skills typically list
ways you can use them even if you’re untrained in the
skill, followed by uses that require you to be trained in the
skill. You can learn more on page 142.
Speed
Speed is the number of feet that a character can move
using the Stride action (see page 310).
Spell
Spells are magical effects generated by using an activity to
cast them. Spells specify what they target, their effects, the
actions needed to cast them, and how they can be resisted.
Learn more on page 192.
Stride
You can move up to your Speed with the Stride action,
which appears on page 308.
Strike
You use the Strike action to make an attack. You can find
it on page 308.
Trait
A trait is an indicator that a rules element obeys special
rules or has certain features. Often, traits have rules
attached that govern how something works. Other times,
they are indicators of how other rules interact with an
ability, creature, item, or other rules element that has that
trait. An index of all the traits used in this book appears
in Appendix A: Traits on page 414.
Turn
During the course of a round, each creature takes a single
turn. A creature can typically use up to 3 actions during
its turn. Learn more on page 304.

