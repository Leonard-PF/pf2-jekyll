---
title: "Créer un personnage"
source: "Playtest Pathfinder"
layout: default
---


DICE
Pathfinder requires a set of polyhedral dice. When these
are mentioned in the text, they’ll have a “d” followed by the
number of sides of the die you’re supposed to use. If you’re
using multiple dice, a number at the start shows how many
you should roll. For example, “4d6” means you’re rolling four
six-sided dice. Pathfinder uses the following dice: 4-sided
(d4), 6-sided (d6), 8-sided (d8), 10-sided (d10), 12-sided (d12),
and 20-sided (d20).

When you sit down to play Pathfinder as a player,
the first thing you need to do is create a character
whose adventures you’ll explore in the game. It’s up
to you to imagine your character’s past experiences,
personality, and worldview, as these will set the stage
for your roleplaying during the game. You’ll use the
game’s mechanics to determine your character’s starting
capabilities at various tasks and the special abilities she
can use during the game.
This section provides a step-by-step guide for creating
a character using the Pathfinder rules. These steps are
presented in a suggested order, but you should feel free
to complete them in the order you prefer. In some cases,
where the rules systems involved in the following steps
need more explanation, they refer to sections that appear
later in this chapter.
Many of the steps below instruct you to fill out
fields on your character sheet (see below), but note
that the character sheet is arranged for ease of use
during gameplay rather than the order of steps in
character creation. Additionally, the fields that appear
on character sheets are comprehensive, and not all
characters necessarily have something to put in each
field—all characters might not have access to a reaction
at 1st level, for example. If a field on your character
sheet is not applicable to your character, just leave that
field blank. Each step of character creation detailed on
the following pages is marked with a corresponding
number on the sample character sheet below to show
you where to fill in information in the appropriate field.
Fields that need to be filled in on the second or third
page of the sheet are called out in the text.
If you’re creating a higher-level character, it’s a good
idea to begin with these instructions before progressing
to page 278 for instructions on leveling up characters.

1 DETERMINE YOUR CHARACTER’S CONCEPT

What sort of hero do you want to play? The answer
to this question might be as simple as “a brave
warrior,” or as complicated as “the child of
elven wanderers who raised this character in a majorityhuman
city devoted to Sarenrae, the Dawnflower and
goddess of the sun.” During this step, it’s a good idea to
decide the general thrust of your character’s personality,
sketching out a few details about her past and thinking
about how and why she adventures. You’ll likely want
to peruse Pathfinder’s available ancestries, backgrounds,
and classes. The tables on page 13 might also help you
match your concept with some basic rules elements.
Below are a number of ways you could approach
creating your character concept. Once you have a good
idea of the type of character you’d like to play, move on
to Step 2.
Ancestry, Background, Class, or Details
If one of Pathfinder’s character ancestries, backgrounds,
or classes particularly intrigues you, it’s easy to build
a character concept around one or more of these
options. Table 1–1: Ancestries and Table 1–2: Classes
on page 13 give a brief overview of each option,
with much more detail provided in Chapters 2 and 3,
respectively. Additionally, the game has a large number
of backgrounds to choose from that represent your
character’s upbringing, her family’s livelihood, or her
earliest profession. Backgrounds are detailed later in
Chapter 2, beginning on page 38.
Building a character around a specific ancestry,
background, or class can be a fun exercise in interacting
with the world’s lore. You might consider whether you’d
like to build a typical member of your character’s ancestry
or class, as described in the relevant entry, or whether
you’d prefer to play a character who defies commonly held
notions about her people. For example, you could play a
dwarf with a wide-eyed sense of wondrous innocence and
zest for change, or a performing rogue capable of amazing
acrobatic feats but with little interest in sneaking about.
Of course, you can always build a concept from any
aspect of a character’s details. You can use roleplaying
to challenge not only the norms of Pathfinder’s fictional
world, but also real-life societal norms. Your character
might challenge binary gender notions, explore cultural
identity, have a disability, have any sexual orientation, or
any combination of these suggestions. Your character can
live any life you see fit.
After you’ve figured out your character’s concept and
background, you can have some fun coming up with an
appropriate name for character!
Faith
Perhaps you’d like to play a character who is a devout
believer in the faith of a specific deity. Pathfinder is a rich
world full of myriad deities whose faiths and philosophies
span a wide range, from Cayden Cailean, the Drunken
Hero of good-hearted adventuring; to Desna, the Song
of Spheres and goddess of dreaming and the stars; to
Iomedae, the Inheritor, goddess of honor, justice, and
rulership. A full list of Pathfinder’s major deities is on page
288. Your character might be so drawn to a particular
faith that you decide she should be a cleric or paladin of
that deity, or your character might instead simply be the
child of strongly devoted parishioners, or a lay worshiper
who applies her faith’s teachings to daily life.
Your Allies
Before a game begins, it’s always a good idea for the
players to discuss how their characters might know each
other and work together throughout the course of their
adventures. You might even want to coordinate with
the other players when forming your character concept,
although you should never feel pressured into making
and playing a character that doesn’t interest you.
Such coordination might involve your characters’
backstories, such as having them hail from the same
village or creating characters who are relatives. It
might also involve mechanical aspects, such as creating
characters whose abilities in combat complement each
other. In the latter case, it can be helpful for a party to
include characters who deal damage, characters who can
absorb damage, and characters who can provide healing.
However, Pathfinder’s classes include a lot of choices, and
there are many options for building each type of character,
so don’t let these broad categories restrain your decisions!
Ability Scores
One of the simplest places to start your character concept
is with their ability scores. Do you want your character
to be the fastest, the smartest, or the most charming?
Then enhance your character’s Dexterity, Intelligence, or
Charisma score accordingly! See the Ability Scores section
on page 18 for more about which ability scores are tied
to which inherent qualities. When you build a character
around having one or more strong ability scores, choose a
class that has one of those as the key ability score. You may
also want to choose an ancestry and a background that also
boost that score, but you can always use your free ability
boosts if they don’t line up. When it comes time to assign
your skill proficiencies, pick skills tied to your best abilities.
CHARAC TER SHEET
Once you’ve developed your character’s concept, jot a few
sentences summarizing it under the Notes section on the
third page. Record any of the details you’ve already decided,
such as your character’s name, on the appropriate lines on
the first page, indicated on the sample character sheet by
the number 1.

ANCESTRIES AND CLASSES
Each player takes a different approach to character creation; some focus on details that best fit the story, some look for combinations
that synergize well mechanically, and others combine aspects of these approaches. There is no wrong way to build a character!
The following tables provide at-a-glance information for those looking to optimize their starting ability scores. For entries in
Table 1–1: Ancestries that say “free,” you can choose which ability score receives the provided ability boost. This table also indicates
any ability flaws that an ancestry might have. You can find more about ability boosts and ability flaws in Ability Scores on page 18.
Table 1–2: Classes lists each class’s key ability score—the ability score used to calculate the potency of many of their class abilities.
Characters receive an ability boost in that ability score when they choose their class. This table also lists one or more secondary
ability scores that can be important to members of that class.
Keep in mind that a character’s background also affects her ability scores, though there’s more flexibility in the ability boosts that
backgrounds provide than in those from classes. For descriptions of the available backgrounds, see page 38.

TABLE 1–1: ANCESTRIES
Ancestry Description Ability Boosts Ability Flaw
Dwarf Dwarves are a short, stocky people who are often stubborn, fierce,
and devoted.
Constitution, Wisdom, Free Charisma
Elf Elves are a tall, slender, long-lived people whose culture peaked
long ago.
Dexterity, Intelligence, Free Constitution
Gnome Gnomes are a short, slight, mercurial people who crave change
and excitement.
Constitution, Charisma, Free Strength
Goblin Goblins are a short, scrappy, energetic people who have spent
millennia maligned and feared.
Dexterity, Charisma, Free Wisdom
Halfling Halflings are a short, adaptable people who exhibit remarkable
curiosity and humor.
Dexterity, Wisdom, Free Strength
Human* Humans are incredibly diverse in terms of everything from their
body size to their perspectives and personalities.
Free, Free —
* Half-elf and half-orc ancestries are accessible through human ancestry feats.
TABLE 1–2: CLASSES
Class Description Key Ability Score* Secondary Ability Scores
Alchemist The alchemist throws alchemical bombs and drinks
concoctions of his own making during combat.
Intelligence Constitution, Dexterity
Barbarian The barbarian flies into a rage on the battlefield, smashing
foes with abandon.
Strength Constitution, Dexterity
Bard Performance and secrets of the occult enable the bard to
distract foes and inspire allies.
Charisma Constitution, Dexterity
Cleric The cleric calls on the power of a deity to cast spells that can
heal allies or harm foes.
Wisdom Charisma, Constitution
Druid The druid uses the natural world’s magic to bolster her and
her allies’ strength while calling pain down upon enemies.
Wisdom Constitution, Dexterity
Fighter The fighter is a master of weapons, martial techniques, and
powerful attack combinations.
Dexterity or Strength Constitution
Monk The monk spins the secrets of martial arts into dazzling
displays of battlefield prowess.
Dexterity or Strength Constitution, Wisdom
Paladin The paladin is a champion of her deity who uses divine power
to enhance her heroics and protect her allies.
Strength Charisma, Constitution
Ranger The ranger is a master of using his surroundings, including
traps and animal allies, to harry enemies.
Dexterity or Strength Constitution, Wisdom
Rogue The rogue is a multitalented master of skullduggery who
strikes when enemies least expect it.
Dexterity Charisma, Constitution
Sorcerer The sorcerer’s magical might flows through her blood and
manifests as fantastic spells and abilities.
Charisma Dexterity, Constitution
Wizard The wizard is an eminent scholar whose reservoirs of arcane
knowledge power his wondrous spells and abilities.
Intelligence Dexterity, Constitution
* Characters each receive an ability boost in their class’s key ability score.

2 CHOOSE AN ANCESTRY
Your character’s ancestry is one of her most important
characteristics. Table 1–1: Ancestries on page 13 provides
an overview of Pathfinder’s core ancestry options, and
each ancestry is fully detailed in Chapter 2. Ancestry
affects your character’s ability scores, total Hit Points,
size, Speed, languages, and much more. Additionally, at
1st level, your character receives an ancestry feat that
represents an ability or quality she was born with or
trained in at an early age.
CHARAC TER SHEET
Write your character’s ancestry on the appropriate line at the
top of your character sheet. Next to your ability scores, note
the ability boosts and any flaw your character gains from her
ancestry (ability scores are finalized during Step 5). Note the
number of Hit Points she gains from her ancestry. Finally, on
the appropriate lines, note your character’s size, Speed, and
languages. If your character’s ancestry provides her with
special abilities, note them in the appropriate spaces, such as
darkvision in the Senses section on the first page. Note the
ancestry feat your character receives in the proper section on
your character sheet’s second page.
3 CHOOSE A BACKGROUND
Your character’s background might represent a special
aptitude she’s been honing since her youth, detail her
upbringing, or illuminate some other aspect of her life
before she became an adventurer. The available character
backgrounds are described beginning on page 38.
Backgrounds typically provide benefits to your character
in the form of two ability boosts (one that can be applied
to either of two specific ability scores, and one that is
free), training in a Lore skill, and a specific skill feat.
CHARAC TER SHEET
Record your character’s background on the appropriate
line on your character sheet. Next to your ability scores,
note the ability boosts the background provides (ability
scores are finalized during Step 5). Record the skill feat
the background provides in the proper section on your
character sheet’s second page. On the first page, in the
Skills section, note the Lore skill in which your character is
trained by checking the “T” box next to that skill name and
noting the type of Lore.
4 CHOOSE A CLASS
At this point, you need to nail down your character’s
class. This affords her access to a suite of heroic abilities,
determines how well she can attack, and governs
how easily she can shake off or avoid certain harmful
effects. Chapter 3 details each of the classes available in
Pathfinder, and Table 1–2: Classes on page 13 provides
an overview of each class and tells you which ability
scores are important when playing that class, which can
help guide your choice. You don’t need to note all of your
character’s class features yet. You simply need to know
which class you want to play, which determines the ability
scores that will be most important for your character.
CHARAC TER SHEET
Write your character’s class at the top of your character
sheet, then write “1” in the box after it to indicate that
you’re 1st level. Next to your ability scores, note the class’s
key ability score and the ability boost to it that the class
provides. Don’t worry about recording the rest of your
character’s class features and abilities yet—you’ll handle
that in Step 6.
5 FINALIZE YOUR ABILITY SCORES
Now that you’ve made the main mechanical choices
about your character, it’s time to finalize her ability
scores. These important statistics determine a wide array
of your character’s capabilities and consist of six values:
Strength, Dexterity, Constitution, Intelligence, Wisdom,
and Charisma. As noted in other steps, your character’s
ancestry, background, and class all affect her ability scores.
To learn how to calculate your character’s ability scores,
see page 19. That section also tells you how to determine
your ability modifiers, which affect the calculation of
many of the values on your character sheet, as described
later in this section.
CHARAC TER SHEET
Once you’ve used the Ability Scores section on page 19 to
calculate your character’s ability scores, record them in the
Score boxes in the proper section on your character sheet’s
first page. Using the ability modifiers table on page 20, write
your character’s modifiers in this section, too.
6 APPLY YOUR CLASS
Now, record all the benefits and class features that your
character receives from the class you’ve chosen. Your
character’s class determines her key ability score, affects
her total Hit Points, outfits her with various initial
proficiencies, provides her with signature skills, and
grants her class features and feats.
To determine your character’s total starting Hit Points,
add together the number of Hit Points she gains from her
ancestry (noted in Step 2) and the number of Hit Points
she gains from her class.
Now is a good time to note the “T,” “E,” “M,” and
“L” fields that appear throughout your character sheet
(standing for trained, expert, master, and legendary,
respectively). These fields indicate your character’s
proficiency and determine which modifier your character
applies to rolls and DCs when using certain mechanics.
For example, if your character has a rank of trained in
a given skill or ability, her modifier to rolls when using
that skill or ability is equal to her level. (For more about
proficiencies, see page 290.)


CHARAC TER SHEET
Note your character’s key ability score on your character
sheet as well as her class DC, which is equal to 10 plus her
key ability modifier plus her level (in this case, 1). Note her
total starting Hit Points as well. Use the proficiency fields
on your character sheet to note your character’s initial
proficiencies, including your character’s proficiency ranks
in Perception and saving throws. Record your character’s
weapon proficiencies in the appropriate section on the first
page, and her armor proficiencies in the Armor Proficiencies
section. Don’t worry yet about finalizing any values for your
character’s AC, TAC, or Strikes—you’ll handle that in Step 9.
Indicate which skills are your character’s signature skills and
choose which skills your character is trained in, but don’t
worry about finalizing the total modifiers for your skills,
either. You’ll handle that in the next step.


7 DETERMINE SKILL MODIFIERS
Now that you’ve noted which skills are signature skills for
your character and decided which skills she’s trained in,
it’s time to calculate her skill modifiers. In the second box
to the right of each skill name on your character sheet,
there’s an abbreviation that reminds you of the ability
score tied to that skill. When you attempt checks using a
skill, add the indicated ability modifier to your proficiency
modifier, if any, as well as any other applicable modifiers,
bonuses, and penalties, to determine the modifier for your
check. Many uses of certain skills require you to have
at least a proficiency rank of trained in those skills, as
further described in Chapter 4.
CHARAC TER SHEET
Fill the relevant ability modifier in the box to the right of
each skill name. Then, for skills in which you are trained,
add your proficiency modifier to your ability modifier to
determine the modifier your character has for each skill
roll. For the rest of your character’s skills, subtract the
proficiency modifier for being untrained (your level – 2)
from the relevant ability modifier, and record those totals
on the lines below the respective skill names. If you have
any modifiers, bonuses, or penalties from feats or abilities,
add them to the totals if they always apply, or note them
next to the total if they apply only in certain circumstances.
8 BUY EQUIPMENT
At 1st level, your character has 150 silver pieces (sp) to
spend on armor, weapons, and other basic equipment.
Armor and weapons are often the most important.
Your character’s class lists the types of weapons
and armor that she is proficient with. Her
weapon determines how much damage she
deals in combat, and her armor influences
her Armor Class, but these calculations are
covered in more detail in Step 9. Don’t neglect essentials
such as food and traveling gear. For more on the equipment
available and how much it costs, see Chapter 6.

CHARAC TER SHEET
Once you’ve spent your character’s starting wealth, note
the equipment she owns as well as any remaining gp, sp, or
cp she might still have. Record your character’s weapons in
the Melee Strikes and Ranged Strikes sections, depending
on the weapon, and the rest of her equipment and her
money in the appropriate section on your character sheet’s
second page. You’ll calculate specific numbers for her
melee Strikes, AC, and TAC in Step 9.

9 FILL IN THE FINISHING DETAILS
Now add the following details to your character sheet on
the appropriate lines.
Age
Note your character’s age. The description for your
character’s ancestry in Chapter 2 gives some guidance
on the age ranges of members of that ancestry. Beyond
that, you can play a character of whatever age you like.
Your character’s age is a major factor that shapes how
she interacts with the world. There aren’t any mechanical
adjustments to your character for being particularly
old, but you might want to take it into account when
considering your starting ability scores and future
advancement; for instance, an old and wise character
might have a higher Wisdom score, so you might want
to make sure to put one of your free ability boosts in
Wisdom. Particularly young characters can change the
tone of some of the game’s threats, so it’s recommended
to play characters who are at least young adults.
Alignment
Your character’s alignment is an indicator of her current
philosophy, behavior, or cosmic status with regard to her
morality and personality tendencies. Her alignment can
change in play as her beliefs change, and you realize that
your character’s actions reflect a different alignment than
the one recorded on your character sheet. In most cases,
you can just change your character’s alignment to match
your new discoveries about your character and continue
playing. If you play a paladin, though, your character’s
alignment must be lawful good (see page 105), and if you
play a cleric, her alignment must be one allowed for her
deity (see page 70); otherwise, your character loses some
of her class abilities until she atones.
There are nine possible alignments in Pathfinder,
as shown on the table. If your alignment has any
components other than neutral, your character gains
traits based on those alignment components. This
might affect the way various spells, items, and creatures
interact with your character.
Your character’s alignment is measured by two sets of
opposed values: the axis of good and evil and the axis of
law and chaos. A character who isn’t committed strongly
to either side is neutral on that axis. Keep in mind that
alignment is a complicated subject, and even acts that
might be considered good can be used for nefarious
purposes, and vice versa. The GM is the final arbiter of
questions about how specific actions might affect your
character’s alignment.
Law and Chaos
Your character has a lawful alignment if she values
consistency, stability, and predictability over flexibility.
Lawful characters have a set system in life, whether it’s
meticulously planning day-to-day activities, carefully
following a set of official or unofficial laws, or strictly
adhering to a code of honor.
On the other hand, if she values flexibility, creativity,
and spontaneity over consistency, she has a chaotic
alignment. That doesn’t mean she decides her life by
choosing randomly with a roll of the dice, however.
Chaotic characters believe that lawful characters are too
inflexible to judge each situation by its own merits or take
advantage of opportunities, while lawful characters believe
that chaotic characters are irresponsible and flighty.
Many characters are in the middle, generally obeying
the law or following a code of conduct in many situations,
but bending the rules when the situation requires it. If your
character is in the middle, too, she is neutral on this axis.
Good and Evil
Your character has a good alignment if she considers the
happiness of others above her own and works selflessly
to assist others, even those who aren’t her friends and
family. She is also good if she values protecting others
from harm, even when it puts her in danger.
She has an evil alignment if she is willing to victimize
others for her own selfish gain, and it’s even more likely
that she’s evil if she enjoys inflicting harm or actively
seeks to harm others.
If your character falls somewhere in the middle, she’s
likely neutral on this axis.
Armor Class (AC and TAC )
Your character has two values to represent how difficult
she is to strike in combat. These are her Armor Class (AC)
and her Touch Armor Class (TAC). Most attacks will be
made against your character’s AC, while those that need
only to touch her to be effective are made against her TAC.
To calculate her AC, add 10 plus her Dexterity
modifier (up to her armor’s Dexterity modifier cap), plus
her proficiency modifier with her armor, plus her armor’s
item bonus to AC and any other bonuses and penalties
that always apply.
To calculate her TAC, add 10 plus her Dexterity
modifier (up to her armor’s Dexterity modifier cap), plus
her proficiency modifier with her armor, plus her armor’s
item bonus to TAC and any other bonuses and penalties
that always apply.
Spells or abilities that give your character a bonus or
penalty to AC also give an equal bonus or penalty to TAC
unless stated otherwise.

TABLE 1–3: THE NINE ALIGNMENTS
Good Neutral Evil
Lawful Lawful good
(LG)
Lawful neutral
(NG)
Lawful evil
(LE)
Neutral Neutral good
(NG)
True neutral
(N)
Neutral evil
(NE)
Chaotic Chaotic good
(CG)
Chaotic neutral
(CN)
Chaotic evil
(CE)

Bulk
Your character’s maximum Bulk determines how much
weight she can comfortably carry. If she is carrying a
total amount of Bulk that equals or exceeds 5 plus her
Strength modifier, she is encumbered. She cannot carry a
total amount of Bulk that exceeds 10 plus her Strength
modifier. Your character’s current Bulk is equal to the sum
of all of her items; keep in mind that every 10 light items
make up 1 Bulk. For more about Bulk, see page 175.
Deity
Note the deity your character worships, if any (clerics and
paladins must worship a deity). See page 288 for more
about Pathfinder’s deities.
Gender
Note your character’s gender, if applicable. Characters of
all genders are equally likely to become adventurers.
Hero Points
Your character usually begins each game session with
1 Hero Point, and can gain additional Hero Points
throughout the course of the game for undertaking helpful
tasks for the group or performing heroic deeds during her
adventures. Your character can use Hero Points to gain
certain benefits, such as staving off death, rerolling a d20,
and the like. See page 300 for more about Hero Points.
Melee Strikes and Ranged Strikes
Next where you’ve noted your character’s melee and
ranged weapons, calculate her modifiers to Strikes with
those weapons and how much damage her Strikes deal.
For modifiers to melee Strikes, add your character’s
proficiency modifier with her weapon plus her Strength
modifier (unless a weapon’s entry says otherwise) plus any
item bonus from the weapon and any other bonuses and
penalties that always apply. To the right of these lines, note
how much damage her melee Strikes deal. This equals your
character’s weapon damage dice plus her Strength modifier.
For modifiers to ranged Strikes, add your character’s
proficiency modifier with her weapon plus her Dexterity
modifier, plus any item bonus from her weapon and
any other bonuses and penalties that always apply.
To the right of these lines, note how much damage
her ranged Strikes deal. For most ranged weapons,
this is equal to your character’s weapon damage
dice. If the weapon has the thrown trait,
also add her Strength modifier. If it has
the propulsive trait, instead add half her
Strength modifier.
Percept ion
Your character’s Perception modifier measures
how alert she is. This number is equal to her proficiency
modifier in Perception plus her Wisdom modifier. For
more about Perception, see page 301.
Resonance Points
Your character has a number of Resonance Points, which
allow her to use magic items. Your character’s total
Resonance Points are equal to her Charisma modifier
plus her level. On the second page, write your character’s
Charisma modifier and level in the appropriate fields. Her
Resonance Point total goes in the fields marked Maximum
and Current. For more on Resonance Points, see page 376.
Saving Throws
From time to time, your character may need to determine
whether she can avoid or shake off an effect or spell. When
this happens, the GM will ask you to attempt a Fortitude,
Reflex, or Will saving throw, depending on the situation. For
each kind of saving throw, add your character’s Fortitude,
Reflex, or Will proficiency modifier (as appropriate) plus
the ability modifier associated with that kind of saving
throw, plus add any modifiers, bonuses, or penalties from
abilities, feats, or items that always apply, then record this
number on the line for that saving throw. For Fortitude
saving throws, use your character’s Constitution modifier.
For Reflex saving throws, use your character’s Dexterity
modifier. For Will saving throws, use your character’s
Wisdom modifier.
